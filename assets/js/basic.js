
(function($) {
    "use strict";
    const cfg = {
            scrollDuration: 800, // smoothscroll duration
        },
        $WIN = $(window);

    const ssPreloader = function () {

        $("html").addClass('ss-preload');
        $WIN.on('load', function () {
            $("#loader").fadeOut("slow", function () {
                $("#preloader").delay(300).fadeOut("slow");
            });

            $("html").removeClass('ss-preload');
            $("html").addClass('ss-loaded');
        });
    };

    /* initialize
     * ------------------------------------------------------ */
    (function ssInit() {
        ssPreloader();
    })();

})(jQuery);

$(document).ready(function(){
    $("#phone").mask("(99) 99999-9999");
});

//Primeiro Formulario
$(document).on('click', '#sub', function(){
    $('#sub').html("Enviando...");
})

$(document).ready(function(){
    $('#agendar').on('submit',function(e) {
        $.ajax({
            url:'forms/agendamento.php',
            data:$(this).serialize(),
            type:'POST',
            success:function(){
                Swal.fire({
                    icon: 'success',
                    title: 'Obrigado',
                    text: 'Em breve entraremos em contato.',
                    showConfirmButton: true,
                    confirmButtonText: 'Confirmar',
                    confirmButtonColor: '#FFB972',
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('#agendar')[0].reset();
                        $("#sub").attr("disabled", true);
                        $('#sub').html("Enviado ✓");
                    }
                })
            },
            error:function(){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Houve um erro, tente novamente mais tarde.',
                    showConfirmButton: true,
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#ff0000',
                })
            }
        });
        e.preventDefault();
    });

});

//Formulario Footer agendar-footer
$(document).on('click', '#sub-2', function(){
    $('#sub-2').html("Enviando...");
})

$(document).ready(function(){
    $('#agendar-footer').on('submit',function(e) {
        $.ajax({
            url:'forms/agendamento-footer.php',
            data:$(this).serialize(),
            type:'POST',
            success:function(){
                Swal.fire({
                    icon: 'success',
                    title: 'Obrigado',
                    text: 'Em breve entraremos em contato.',
                    showConfirmButton: true,
                    confirmButtonText: 'Confirmar',
                    confirmButtonColor: '#FFB972',
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('#agendar-footer')[0].reset();
                        $("#sub-2").attr("disabled", true);
                        $('#sub-2').html("Enviado ✓");
                    }
                })
            },
            error:function(){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Houve um erro, tente novamente mais tarde.',
                    showConfirmButton: true,
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#ff0000',
                })
            }
        });
        e.preventDefault();
    });

});