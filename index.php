<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Rede n°1 em reconectar pessoas a sua autoestima. Especializada em Lifting e Estética Facial ✨">

        <link rel="stylesheet" href="https://clientes.g6publicidade.com/LiftOne-Londrina/assets/css/style.scss">
        <link rel="stylesheet" href="assets/fonts/fonts.css">

        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/pace.min.js"></script>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>

        <script src="https://kit.fontawesome.com/06b4f47648.js" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.2/dist/sweetalert2.all.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>


        <title>LiftOne Londrina</title>
        <link rel="shortcut icon"  href="assets/imgs/fav-62x62.png" />

    </head>
    <body>

        <nav class="navbar navbar-light">
            <div class="container">
                <img class="img-fluid" src="assets/imgs/nav-logo.png" alt="">
                <div class="mr-5">
                    <a href="https://www.instagram.com/liftonelondrina/" target="_blank">
                        <img class="img-fluid" src="assets/imgs/ico-ig.png" alt="">
                    </a>
                </div>
            </div>
        </nav>


        <section id="Home">
            <div class="container">
                <div class="offset-md-6 col-md-5">
                    <div class="form-box p-md-4 p-3">
                        <form id="agendar" class="text-light p-md-3 p-3">
                            <h1 class="pt-4 pb-3 text-center">
                                Sinta o poder da
                                autoestima com a
                                Lift ONE
                            </h1>
                            <h5 class="font-weight-normal text-center pt-3 pb-2">Agende uma avaliação:</h5>
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Nome" aria-label="Nome" required>
                            </div>
                            <div class="form-group mb-1">
                                <input type="tel" class="form-control" id="phone" name="number" placeholder="Número de telefone" aria-label="Telefone" required>
                            </div>

                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="WhatsApp" >
                                <label class="form-check-label" for="inlineRadio1"><span class="mini-text">WhatsApp</span></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Telefone">
                                <label class="form-check-label" for="inlineRadio2"><span class="mini-text">Telefone</span></label>
                            </div>

                            <div class="form-group pt-3">
                                <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Cidade" aria-label="Cidade" required>
                            </div>

                            <h5 class="text-center">Tratamento desejado:</h5>
                            <div class="form-group">
                                <input type="text" class="form-control" id="tratamento" name="tratamento" aria-label="tratamento" required>
                            </div>

                            <div class="form-group text-center pb-4">
                                <button id="sub" type="submit" class="btn col-12">Enviar</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </section>


        <section id="Segredo">
            <div class="container text-light pb-4">
                <h1 class="text-center pt-5">
                    Qual o segredo dos tratamentos <br>
                    Lift ONE?
                </h1>
                <h4 class="text-justify font-weight-normal p-md-5 pt-md-0 pt-4 ml-md-5 ml-2 mr-md-5 mr-2">
                    A base do nosso trabalho está na qualificação dos nossos profissionais! Ter especialistas
                    capacitados para atender cada um de nossos casos, com a exclusividade e
                    personalização que você merece, é um dos nossos principais compromissos.
                    <br><br>
                    É por isso que nossa rede conta com a experiência do Dr. Raphael Alves, criador da
                    técnica do 5º ponto, também conhecido como FULL FACE LIFTING, garantindo
                    qualidade, mantendo o compromisso e o cuidado que você merece.
                    <br><br>
                    E aqui em Londrina, para oferecer tratamentos com ainda mais exclusividade, os
                    procedimentos são supervisionados pela Dra. Anna Karina Pansardi, há mais de 25 anos
                    especialista na área.
                </h4>
            </div>
        </section>


        <section id="Carousel" class="master-index">
            <div class="container pt-5 pb-2">

                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                                <img class="col-4" src="assets/imgs/carousel/1.png" alt="First slide">
                                <img class="col-4" src="assets/imgs/carousel/2.png" alt="First slide">
                                <img class="col-4" src="assets/imgs/carousel/3.png" alt="First slide">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <img class="col-4" src="assets/imgs/carousel/4.png" alt="First slide">
                                <img class="col-4" src="assets/imgs/carousel/1.png" alt="First slide">
                                <img class="col-4" src="assets/imgs/carousel/2.png" alt="First slide">
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <ol class="carousel-indicators pt-2">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    </ol>
                </div>

                <div class="col-12">
                    <h1>Antes & Depois</h1>
                </div>

            </div>
        </section>


        <section id="Tratamentos">
            <div class="container text-light pt-5 pb-5">
                <h1 class="text-center">
                    O que nossos tratamentos podem<br>
                    fazer por você:
                </h1>
                <h4 class="text-justify font-weight-normal p-md-5 pt-md-0 pb-md-3 pb-4 pt-4 ml-md-5 ml-2 mr-md-5 mr-2">
                    Com tratamentos simples e inovadores em minutos vamos recuperar a sua autoestima e
                    confiança. Temos procedimentos com resultados imediatos, sem cicatriz e sem dor
                    alguma. Além disso, você vai se surpreender com nossa estrutura e recuperação quase
                    que instantânea.
                </h4>

                <h3 class="text-center">
                    • Rejuvenescimento da pele;&nbsp;&nbsp;&nbsp;&nbsp; • Redução da flacidez; <br>
                    • Redução das rugas;&nbsp;&nbsp;&nbsp; • Melhora do contorno facial; <br>
                    • E muito mais!
                </h3>
            </div>
        </section>


        <section id="Whats-Banner">
            <div class="container-fluid p-0">
                <a href="https://api.whatsapp.com/send?phone=5543991389953&amp;text=Ol%C3%A1,%20estava%20no%20site%20da%20LiftOne%20Londrina%20e%20gostaria%20de%20saber%20mais%20sobre%20os%20tratamentos.">
                    <img class="img-fluid" src="assets/imgs/whats-banner.png" alt="">
                </a>
            </div>
        </section>


        <section id="Videos">
            <div class="container pt-5 pb-2">
                <div id="carouselExampleControls" class="carousel slide pt-5 pb-2 pr-md-5 pl-md-5" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <a href="https://www.youtube.com/" target="_blank">
                                <img class="d-block w-100" src="assets/imgs/video-thumb.png" alt="First slide">
                            </a>
                        </div>
                        <div class="carousel-item">
                            <a href="https://www.youtube.com/" target="_blank">
                                <img class="d-block w-100" src="assets/imgs/video-thumb.png" alt="First slide">
                            </a>
                        </div>
                        <div class="carousel-item">
                            <a href="https://www.youtube.com/" target="_blank">
                                <img class="d-block w-100" src="assets/imgs/video-thumb.png" alt="First slide">
                            </a>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>

                    <div class="col-12 p-0">
                        <h1>Histórias Lift ONE:</h1>
                    </div>
                </div>
            </div>
        </section>


        <section id="Contato">
            <div class="container">
                <div class="offset-md-6 col-md-5">
                    <div class="form-box p-md-4 p-3">
                        <form id="agendar-footer" class="text-light p-md-3 p-3">
                            <h4 class="pt-4 pb-3 text-center">
                                E aí, vamos dar aquele up na sua
                                autoestima, começando agora?
                            </h4>
                            <h5 class="font-weight-normal text-center pt-3 pb-2">
                                Preencha com seus dados para
                                nossa equipe entrar em contato:
                            </h5>
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name2" placeholder="Nome" aria-label="Nome" required>
                            </div>
                            <div class="form-group mb-1">
                                <input type="tel" class="form-control" id="phone" name="number2" placeholder="Número de telefone" aria-label="Telefone" required>
                            </div>

                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="WhatsApp">
                                <label class="form-check-label" for="inlineRadio1"><span class="mini-text">WhatsApp</span></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Telefone">
                                <label class="form-check-label" for="inlineRadio2"><span class="mini-text">Telefone</span></label>
                            </div>

                            <div class="form-group pt-3">
                                <input type="text" class="form-control" id="cidade" name="cidade2" placeholder="Cidade" aria-label="Cidade" required>
                            </div>

                            <h5 class="text-center">Tratamento desejado:</h5>
                            <div class="form-group">
                                <input type="text" class="form-control" id="tratamento" name="tratamento2" aria-label="tratamento" required>
                            </div>

                            <div class="form-group text-center pb-4">
                                <button id="sub-2" type="submit" class="btn col-12">Enviar</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </section>


        <footer>
            <div class="container pt-2 pb-2">
                <div class="row my-auto text-md-left text-center">
                    <div class="col-md-6 pb-md-0 pb-3">
                        <a href="https://goo.gl/maps/TW5bJYhayhm1Ei6H6" target="_blank">
                            <p class="m-0">
                                <img class="img-fluid pr-2" src="assets/imgs/ico-place.png" alt="">
                                Rua Caracas, nº 21, Bairro Guanabara - Londrina/PR
                            </p>
                        </a>
                    </div>
                    <div class="col-md-4 pb-md-0 pb-3">
                        <a href="https://api.whatsapp.com/send?phone=5543991389953&amp;text=Ol%C3%A1,%20estava%20no%20site%20da%20LiftOne%20Londrina%20e%20gostaria%20de%20saber%20mais%20sobre%20os%20tratamentos." target="_blank">
                            <p class="m-0">
                                <img class="img-fluid pr-1" src="assets/imgs/ico-wts.png" alt="">
                                (43) 9 9138-9953
                            </p>
                        </a>

                    </div>
                    <div class="col-md-2 pb-md-0 pb-3">
                        <a href="https://www.facebook.com/liftonelondrina" target="_blank"><img class="img-fluid pr-2" src="assets/imgs/ico-fb.png" alt=""></a>
                        <a href="https://www.instagram.com/liftonelondrina/" target="_blank"><img class="img-fluid" src="assets/imgs/ico-ig.png" alt=""></a>
                    </div>
                </div>
            </div>
        </footer>


        <div id="whatsapp-container" class="box bounce-6">
            <a aria-label="Whatsapp" rel="noreferrer noopener" target="_blank"
               href="https://api.whatsapp.com/send?phone=5543991389953&amp;text=Ol%C3%A1,%20estava%20no%20site%20da%20LiftOne%20Londrina%20e%20gostaria%20de%20saber%20mais%20sobre%20os%20tratamentos.">
                <i class="fa fa-whatsapp" aria-hidden="true"></i>
            </a>
        </div>

        <div id="preloader">
            <div id="loader">
                <div class="col-12 pb-2">
                    <img class="img-fluid" src="assets/imgs/nav-logo.png" alt="">
                </div>
                <div class="line-scale-pulse-out">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>

        <script src="assets/js/basic.js"></script>
    </body>
</html>